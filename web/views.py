from django.shortcuts import render
from django.contrib.auth import authenticate,login,logout
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.http import HttpResponse,HttpResponseRedirect
from .models import *
import numpy as np
from sklearn.externals import joblib
import os
etr=joblib.load('spaps_research/train/etr0.85.pkl')

def index(request):
    return render(request,'index.html')

@csrf_protect
def train(request):
    if request.user.is_authenticated:
        if request.method == 'GET':
            return render(request,'train.html',{'user':request.user,'title':'Train model'})
        elif request.method == 'POST':
            file =  request.FILES.get("file")
            l=[list(map(bytes.decode,i.split(b','))) for i in file.readlines()]
            return render(request,'tables.html',{'user':request.user,'title':'Train model','headl':l[0],'bodyl':l[1:]})
    else:
        return render(request,'login.html',{'msg':'Please login first'})

@csrf_protect
def test(request):
    if request.user.is_authenticated:
        if request.method == 'GET':
            return render(request,'predict.html',{'user':request.user,'title':'Predict Results'})
        elif request.method == 'POST':
            name = request.POST.get('name')
            typ = request.POST.get('typ')
            lab = request.POST.get('lab')
            ser1 = request.POST.get('ser1')
            ser2 = request.POST.get('ser2')
            internal = request.POST.get('int')
            mser1 = request.POST.get('mser1')
            mser2 = request.POST.get('mser2')
            minternal = request.POST.get('mint')
            print(ser1,mser1)
            ser1=float(ser1)/float(mser1)
            ser2 = float(ser2)/float(mser2)
            inter = float(internal)/float(minternal)
            attend = (inter*50-10-ser1*15-ser2*15)/10.0
            features = [int(typ),int(lab),inter,ser1,ser2,attend]
            print(ser1,ser2,features)
            if lab=='1':
                msg1 = "Practice more on the lab. If he/she continues in this pace it will be difficult for him/her to pass the exam."
                msg2 = "He/She is doing ok. If he/she continue in this pace, will pass exam. Practice more on the lab to attain more marks."
                msg3 = "He/she has got excellent marks. Practice more to become a rank holder."
            else:
                if typ=='1':
                    msg1 = "Try out more problems. If he/she continues in this pace it will be difficult for him/her to pass the exam."
                    msg2 = "He/she are doing ok. If continued in this pace he/she will get pass marks. Try out more problems to get more marks."
                    msg3 = "He/she has got excellent marks. Do more problems to become a rank holder."
                else:
                    msg1 = "Note down points and study more theory. If he/she continues in this pace it will be difficult to pass the exam. "
                    msg2 = "He/she are doing ok. If continued in this pace he/she will get pass marks. Note down points and study more theory to attain more marks."
                    msg3 = "He/she has got excellent marks. Study more to become a rank holder."
            pred = np.ravel(etr.predict([features]))[0]
            if pred < 0.45:
                msg = msg1
                print(pred)
            elif pred < 0.70:
                msg=msg2
                print(pred)
            else:
                msg=msg3
                print(pred)
            return render(request,'predicted.html',{'user':request.user,'title':'Train model','marks':round(pred*100),'error':'1.688','msg':msg,'name':name})
        else:
            return HttpResponseRedirect('/home/')
    else:
        return render(request,'login.html',{'msg':'Please login first'})
    

@csrf_protect
def login_user(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/home/')
    if request.method == 'GET':
        return render(request,'login.html',{})
    if request.method ==  'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        try:
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request,user)
                return HttpResponseRedirect('/home/')
            else :
                raise Exception
        except Exception as e:
            return render(request,'login.html',{'msg':'Invalid user name or password'})

def logout_user(request):
    logout(request)
    return render(request,'login.html',{'msg':'Logged out successfully'})

@csrf_protect
def home(request):
    if request.method == 'GET':
        if request.user.is_authenticated:
            return render(request,'dashboard.html',{'user':request.user,'title':'Home'})
        else:
            return render(request,'login.html',{'msg':'Please login first'})
