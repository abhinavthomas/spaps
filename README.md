## Student Performance Analysis and Prediction System

Studentsí performance is a key factor in the development of any educational institution. It is through their performance that the institution gains recognition. Every institution has the data of all their students right from their first internal exam, to their last University exam. Educational Data Mining, Data Analytics and Machine Learning are important areas of research, which can be used to retrieve useful knowledge from educational databases. The project is to build an analytical tool that will predict the different performance parameter of a new batch of students based on the data available. Different performance parameters are pass percentage of a batch, performance of a student etc.
The aim is to create a tool which will help the institution to analyse the data and predict the performance of students.

* The project contains a web application to input the marks and predict the mfinal semester marks using the trained models.

---

## Background research proposed

> Large-scale empirical comparison of eight supervised learning algorithms. We evaluate the performance of SVM, neural nets, linear regression, Naive Bayes, memory-based learning, random forests, decision trees and boosted trees on Academic data problem. For each algorithm, we examine different variations and thoroughly explore the space of parameters. For example, we compare ten decision tree styles, neural nets of many sizes, SVMs with many kernels, etc. using a tree-based optimisation technique (TPOT).

![Mean Square Error Comparison](https://bitbucket.org/abhinavthomas/spaps/raw/fde03bd0e1aad7566d7ed07e64d87db81b4baf9e/web/static/images/final_plot.png)

* Blue colour represents decision tree.
* Pink colour represents linear regression
* Green colour represents random forest
* Grey colour represents SVM
* Brown colour represents Naive Bayes
* Black colour represents KNN
* Orange colour represents MLP
* Magenta colour represents Gradient Boost

---