from bs4 import BeautifulSoup as bs
import os
import csv
import xlrd
import string
# import json
import pickle
from collections import OrderedDict

li_final = os.listdir('result/')
li_series = os.listdir('final/')
li_final.sort()
li_series.sort()
data=OrderedDict()
target=OrderedDict()
no_subs={'3':[6,2],'4':[6,2],'5':[6,2],'6':[6,2],'7':[6,2,2],'8':[5,1,1]}

with open('sems.csv', 'rb') as f:
    reader = csv.reader(f)
    your_list = list(reader)


subj = {}

for i in your_list:
	subj[i[0]]=(1 if i[1]=="TRUE" else 0,1 if i[2]=="TRUE" else 0)

for final in li_final:
	base_name = final.split('.')[0]
	sem = base_name[5]
	ser1 = base_name+'_series1.xls'
	ser2 = base_name+'_series2.xls'

	ser1 = ser1 if ser1 in li_series else base_name+'_series1.xlsx'
	ser2 = ser2 if ser2 in li_series else base_name+'_series2.xlsx'



	fp_final =  open('result/'+final,'r')
	
	l1=bs(fp_final.read())("tr")

	print final
	tds=l1[1]('td')[0]("tr")[0]
	head = [i.text.split('\n')[3].strip("\t") if i.text[0]=='\n' else i.text for i in tds("td")]
	for row in tds.fetchNextSiblings():
		try:
			temp1=[]
			for i in row("td"):
				try:
					if i.text[0]=='\n':
						temp1.append(i.text.split('\n')[3].strip("\t"))
					else:
						temp1.append(i.text)
				except IndexError as e:
					temp1.append(i.text)

			if len(temp1)!=28:
				pass
			else:
				for x in xrange(4,26,3):			
					try:
						if string.replace(temp1[x],u'\xa0',u'') == u'':
							temp1[x]=0
						if string.replace(temp1[x+2],u'\xa0',u'') == u'':
							temp1[x+2]=0
						
						if int(temp1[x])>50 and int(temp1[x+2])==0:
							temp1[x+2]=temp1[x]
							temp1[x]=int(temp1[x])/2

						data[string.replace(string.replace(temp1[1],u'\xa0',u''),u' ',u'')+'_'+base_name[:4]+'_'+head[x]] = [subj[head[x]][0],subj[head[x]][1],float(temp1[x])/50.0]
						target[string.replace(string.replace(temp1[1],u'\xa0',u''),u' ',u'')+'_'+base_name[:4]+'_'+head[x]] = [float(temp1[x+2])/100.0]
					except Exception as e:
						raise e
		except Exception as e:
			raise e

	sheet1 = xlrd.open_workbook('final/'+ser1).sheet_by_index(0)
	li1=[i for i in sheet1.get_rows()]
	sheet2 = xlrd.open_workbook('final/'+ser2).sheet_by_index(0)
	li2=[i for i in sheet2.get_rows()]
	head=li1[3]

	for sub in xrange(2,no_subs[sem][0]+2):
		ser_l=[]
		for i in sheet2.col(sub)[5:]:
			if type(i.value)==float:
				ser_l.append(i.value)
			else:
				try:
					val = string.replace(i.value,u'\xa0',u'')
					ser_l.append(float(val) if val!=u'' else 0.0)
				except Exception as e:
					break
		div_ser = (100,20) if max(ser_l)>50 else (50,15)

		for i in xrange(5,len(li1)):
			if type(li1[i][0].value)!=float:
				break
			else:
				if type(li1[i][sub].value)==float:
					val1 = li1[i][sub].value
				else:
					val1 = string.replace(li1[i][sub].value,u'\xa0',u'')
					val1 = float(val1) if val1!=u'' else 0.0
				if type(li2[i][sub].value)==float:
					val2 = li2[i][sub].value
				else:
					val2 = string.replace(li2[i][sub].value,u'\xa0',u'')
					val2 = float(val2) if val2!=u'' else 0.0
				try:
					temp = data[string.replace(string.replace(li1[i][1].value,u'\xa0',u''),u' ',u'')+'_'+base_name[:4]+'_'+head[sub].value]
					attendance = (temp[2]*50-10-(val2*div_ser[1]/div_ser[0])-(val1*(30-div_ser[1])/50.0))/10
					temp.extend([val1/50.0,val2/div_ser[0],attendance])
					data[string.replace(string.replace(li1[i][1].value,u'\xa0',u''),u' ',u'')+'_'+base_name[:4]+'_'+head[sub].value] = temp
				except KeyError as e:
					with open('missing.csv', 'a+') as csv_file:
						writer = csv.writer(csv_file, delimiter='\n')
						writer.writerow([string.replace(string.replace(li1[i][1].value,u'\xa0',u''),u' ',u'')+'_'+base_name[:4]+'_'+head[sub].value])

	for sub in xrange(no_subs[sem][0]+2,no_subs[sem][0]+2+no_subs[sem][1]):
		for i in xrange(5,len(li1)):
			if type(li1[i][0].value)!=float:
				break
			else:
				if type(li1[i][sub].value)==float:
					val1 = li1[i][sub].value
				else:
					val1 = string.replace(li1[i][sub].value,u'\xa0',u'')
					val1 = float(val1) if val1!=u'' else 0.0
				if type(li2[i][sub].value)==float:
					val2 = li2[i][sub].value
				else:
					val2 = string.replace(li2[i][sub].value,u'\xa0',u'')
					val2 = float(val2) if val2!=u'' else 0.0
				try:
					if val1==0.0 and val2 ==0.0:
						print "val1 and val2 is 0 at : "+string.replace(string.replace(li1[i][1].value,u'\xa0',u''),u' ',u'')+'_'+base_name[:4]+'_'+head[sub].value
					else:
						val1/=25
						if val2 == 0.0:
							val2 = val1

					temp = data[string.replace(string.replace(li1[i][1].value,u'\xa0',u''),u' ',u'')+'_'+base_name[:4]+'_'+head[sub].value]
					attendance = (temp[2]*50-(val2*20)-(val1*20))/10
					temp.extend([val1,val2,attendance])
					data[string.replace(string.replace(li1[i][1].value,u'\xa0',u''),u' ',u'')+'_'+base_name[:4]+'_'+head[sub].value] = temp
				except KeyError as e:
					with open('missing.csv', 'a+') as csv_file:
						writer = csv.writer(csv_file, delimiter='\n')
						writer.writerow([string.replace(string.replace(li1[i][1].value,u'\xa0',u''),u' ',u'')+'_'+base_name[:4]+'_'+head[sub].value])

	fp_final.close()
pickle.dump( data, open( "data.p", "wb" ))
pickle.dump( target, open( "target.p", "wb" ))