import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
from sklearn.externals import joblib
from sklearn.metrics import mean_squared_error, r2_score, accuracy_score
from sklearn.model_selection import train_test_split
from collections import OrderedDict
import pickle
import os

t1=OrderedDict()

d1=OrderedDict()

t1=pickle.load(open('target.p','rb'))

d1=pickle.load(open('data.p','rb'))

train_sizes = np.arange(0.1,0.95,0.05)
lr_errors=[]
dt_errors=[]
rf_errors=[]
svr_errors=[]
br_errors=[]
mlp_errors=[]
gb_errors=[]
knn_errors=[]
etr_errors=[]

os.chdir('train')
for train_sz in train_sizes:
	X_train, X_test, y_train, y_test = train_test_split(d1.values(),t1.values(), train_size=train_sz, random_state=42)

	#preprocess y_train and y_test to make it list from list of lists

	y_train = np.ravel(y_train)

	y_test = np.ravel(y_test)



	"""  Linear Regression """
	print('lr'+str(train_sz))
	lr = linear_model.LinearRegression()

	# Train the model using the training sets
	lr.fit(X_train,y_train)

	joblib.dump(lr,'lr'+str(train_sz)+'.pkl')

	y_pred = np.ravel(lr.predict(X_test))

	lr_errors.append(mean_squared_error(y_test,y_pred))

	""" Decision Tree"""

	print('dt'+str(train_sz))
	from sklearn.tree import DecisionTreeRegressor

	dt = DecisionTreeRegressor(random_state=0)

	dt.fit(X_train, y_train)

	joblib.dump(dt,'dt'+str(train_sz)+'.pkl')

	y_pred = np.ravel(dt.predict(X_test))

	dt_errors.append(mean_squared_error(y_test,y_pred))

	
	print('rf'+str(train_sz))
	"""  Random Forest """
	from sklearn.ensemble import RandomForestRegressor

	rf = RandomForestRegressor(max_depth=5, random_state=0)

	rf.fit(X_train, y_train)

	joblib.dump(rf,'rf'+str(train_sz)+'.pkl')

	y_pred = np.ravel(rf.predict(X_test))

	rf_errors.append(mean_squared_error(y_test,y_pred))

	

	print('svm'+str(train_sz))
	""" SVM """
	from sklearn.svm import SVR

	svr = SVR(C=1.0, epsilon=0.2)

	svr.fit(X_train, y_train)

	joblib.dump(svr,'svr'+str(train_sz)+'.pkl')
	
	y_pred = np.ravel(svr.predict(X_test))

	svr_errors.append(mean_squared_error(y_test,y_pred))


	print('knn'+str(train_sz))
	""" KNN """
	from sklearn.neighbors import KNeighborsRegressor

	knn = KNeighborsRegressor(n_neighbors=5)
	knn.fit(X_train, y_train)

	joblib.dump(knn,'knn'+str(train_sz)+'.pkl')

	y_pred = np.ravel(knn.predict(X_test))

	knn_errors.append(mean_squared_error(y_test,y_pred))

	

	print('br'+str(train_sz))
	""" Bayesian """
	from sklearn.linear_model import BayesianRidge

	br = BayesianRidge(compute_score=True)
	br.fit(X_train, y_train)

	joblib.dump(br,'br'+str(train_sz)+'.pkl')

	y_pred = np.ravel(br.predict(X_test))

	br_errors.append(mean_squared_error(y_test,y_pred))


	""" MLP """
	print('mlp'+str(train_sz))
	from sklearn.neural_network import MLPRegressor
	mlp = MLPRegressor(
	    hidden_layer_sizes=(10,10,10,10,),  activation='relu', solver='adam', alpha=0.001, batch_size='auto',
	    learning_rate='adaptive', learning_rate_init=0.01, power_t=0.5, max_iter=1000, shuffle=True,
	    random_state=9, tol=0.0001, verbose=False, warm_start=False, momentum=0.9, nesterovs_momentum=True,
	    early_stopping=False, validation_fraction=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08)

	mlp.fit(X_train, y_train)

	joblib.dump(mlp,'mlp'+str(train_sz)+'.pkl')

	y_pred = np.ravel(mlp.predict(X_test))

	mlp_errors.append(mean_squared_error(y_test,y_pred))

	

	""" Gradiant Basis"""

	print('gb'+str(train_sz))
	from sklearn.ensemble import GradientBoostingRegressor

	params = {'n_estimators': 500, 'max_depth': 4, 'min_samples_split': 2,
	          'learning_rate': 0.01, 'loss': 'ls'}

	gb = GradientBoostingRegressor(**params)

	gb.fit(X_train, y_train)

	joblib.dump(gb,'gb'+str(train_sz)+'.pkl')

	y_pred = np.ravel(gb.predict(X_test))

	gb_errors.append(mean_squared_error(y_test,y_pred))


	""" Extra Trees Regressor"""

	print('etr'+str(train_sz))

	from sklearn.ensemble import ExtraTreesRegressor

	etr = ExtraTreesRegressor(bootstrap=True, max_features=0.9, min_samples_leaf=18, min_samples_split=17, n_estimators=100)

	etr.fit(X_train, y_train)

	joblib.dump(etr,'etr'+str(train_sz)+'.pkl')

	y_pred = np.ravel(etr.predict(X_test))

	etr_errors.append(mean_squared_error(y_test,y_pred))


plt.plot(train_sizes, lr_errors, 'pink', train_sizes, dt_errors, 'blue',
	 train_sizes,rf_errors, 'green',train_sizes,svr_errors,'gray',
	 train_sizes,br_errors,'brown',train_sizes,knn_errors,'black',
	 train_sizes,mlp_errors,'orange',train_sizes,gb_errors,'cyan',
	 train_sizes,etr_errors,'magenta')
plt.grid()
plt.show()