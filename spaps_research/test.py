import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
from sklearn.externals import joblib
from sklearn.metrics import mean_squared_error, r2_score, accuracy_score
from sklearn.model_selection import train_test_split
from collections import OrderedDict
import pickle
import os


t1=OrderedDict()

d1=OrderedDict()

t1=pickle.load(open('target.p','rb'))

d1=pickle.load(open('data.p','rb'))

train_sizes = np.arange(0.1,0.95,0.05)
lr_errors=[]
dt_errors=[]
rf_errors=[]
svr_errors=[]
br_errors=[]
mlp_errors=[]
gb_errors=[]
knn_errors=[]
etr_errors=[]

os.chdir('train')
for train_sz in train_sizes:
	X_train, X_test, y_train, y_test = train_test_split(d1.values(),t1.values(), train_size=train_sz, random_state=42)

	#preprocess y_train and y_test to make it list from list of lists

	y_train = np.ravel(y_train)

	y_test = np.ravel(y_test)

	"""  Linear Regression """
	print('gb'+str(train_sz))
	lr=joblib.load('lr'+str(train_sz)+'.pkl')

	y_pred = np.ravel(lr.predict(X_test))

	lr_errors.append(mean_squared_error(y_test,y_pred))

	""" Decision Tree"""

	print('dt'+str(train_sz))

	dt=joblib.load('dt'+str(train_sz)+'.pkl')

	y_pred = np.ravel(dt.predict(X_test))

	dt_errors.append(mean_squared_error(y_test,y_pred))

	
	print('rf'+str(train_sz))
	"""  Random Forest """

	rf=joblib.load('rf'+str(train_sz)+'.pkl')

	y_pred = np.ravel(rf.predict(X_test))

	rf_errors.append(mean_squared_error(y_test,y_pred))

	

	print('svm'+str(train_sz))
	""" SVM """

	svr=joblib.load('svr'+str(train_sz)+'.pkl')
	
	y_pred = np.ravel(svr.predict(X_test))

	svr_errors.append(mean_squared_error(y_test,y_pred))


	print('knn'+str(train_sz))
	""" KNN """

	knn=joblib.load('knn'+str(train_sz)+'.pkl')

	y_pred = np.ravel(knn.predict(X_test))

	knn_errors.append(mean_squared_error(y_test,y_pred))

	

	print('br'+str(train_sz))
	""" Bayesian """

	br = joblib.load('br'+str(train_sz)+'.pkl')

	y_pred = np.ravel(br.predict(X_test))

	br_errors.append(mean_squared_error(y_test,y_pred))


	""" MLP """
	print('mlp'+str(train_sz))
	mlp=joblib.load('mlp'+str(train_sz)+'.pkl')

	y_pred = np.ravel(mlp.predict(X_test))

	mlp_errors.append(mean_squared_error(y_test,y_pred))

	

	""" Gradiant Basis"""

	print('gb'+str(train_sz))

	gb=joblib.load('gb'+str(train_sz)+'.pkl')

	y_pred = np.ravel(gb.predict(X_test))

	gb_errors.append(mean_squared_error(y_test,y_pred))


	""" Extra Tree Regressor"""
	print('etr'+str(train_sz))

	etr=joblib.load('etr'+str(train_sz)+'.pkl')

	y_pred = np.ravel(etr.predict(X_test))

	etr_errors.append(mean_squared_error(y_test,y_pred))

plt.grid()

plt.plot(train_sizes, lr_errors, 'pink', train_sizes, dt_errors, 'blue',
	 train_sizes,rf_errors, 'green',train_sizes,svr_errors,'gray',
	 train_sizes,br_errors,'brown',train_sizes,knn_errors,'black',
	 train_sizes,mlp_errors,'orange',train_sizes,gb_errors,'cyan',
	 train_sizes,etr_errors,'magenta')

plt.show()