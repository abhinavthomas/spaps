from tpot import TPOTRegressor
import numpy as np
from sklearn.model_selection import train_test_split
from collections import OrderedDict
import pickle

t1=OrderedDict()

d1=OrderedDict()

target=pickle.load(open('target.p','rb'))

data=pickle.load(open('data.p','rb'))

for i in data:
	if len(data[i])>5:
		d1[i]=data[i]
		t1[i]=target[i]

X_train, X_test, y_train, y_test = train_test_split(d1.values(),t1.values(), test_size=0.25, random_state=42)

#preprocess y_train and y_test to make it list from list of lists

y_train = np.ravel(y_train)

y_test = np.ravel(y_test)

X_train = np.array(X_train)

X_test = np.array(X_test)

tpot = TPOTRegressor(generations=5, population_size=50, verbosity=2)

tpot.fit(X_train, y_train)

print(tpot.score(X_test, y_test))

tpot.export('tpot_spaps_pipeline.py')
