import os
import csv
import xlrd
import string
import pickle
from collections import OrderedDict

data=OrderedDict()
target=OrderedDict()

li_series = os.listdir('final/')
li_series.sort()
list_final = os.listdir('internalexternal/')

no_subs={'3':[6,2],'4':[6,2],'5':[6,2],'6':[6,2],'7':[6,2,2],'8':[5,1,1]}

li_final = list(set([i.split('_')[0] for i in list_final]))
li_final.sort()

with open('sems.csv', 'rb') as f:
    reader = csv.reader(f)
    your_list = list(reader)

subj = {}

for i in your_list:
	subj[i[0]]=(1 if i[1]=="TRUE" else 0,1 if i[2]=="TRUE" else 0)

for final in li_final:
	print(final)
	sem = final[5]
	ser1 = final+'_series1.xls'
	ser2 = final+'_series2.xls'
	ser1 = ser1 if ser1 in li_series else final+'_series1.xlsx'
	ser2 = ser2 if ser2 in li_series else final+'_series2.xlsx'


	inter = final+'_internal.xls'
	ext = final+'_external.xls'
	inter = inter if inter in list_final else final+'_internal.xlsx'
	ext = ext if ext in list_final else final+'_external.xlsx'

	sheet1 = xlrd.open_workbook('internalexternal/'+inter).sheet_by_index(0)
	internal=[i for i in sheet1.get_rows()]
	sheet2 = xlrd.open_workbook('internalexternal/'+ext).sheet_by_index(0)
	external=[i for i in sheet2.get_rows()]
	sheet3 = xlrd.open_workbook('final/'+ser1).sheet_by_index(0)
	series1=[i for i in sheet3.get_rows()]
	sheet4 = xlrd.open_workbook('final/'+ser2).sheet_by_index(0)
	series2=[i for i in sheet4.get_rows()]
	head=internal[3]

	for sub in xrange(2,no_subs[sem][0]+2):
		ser_l=[]
		for i in sheet4.col(sub)[5:]:
			if type(i.value)==float:
				ser_l.append(i.value)
			else:
				try:
					val = string.replace(i.value,u'\xa0',u'')
					ser_l.append(float(val) if val!=u'' else 0.0)
				except Exception as e:
					break
		div_ser = (100,20) if max(ser_l)>50 else (50,15)
		print(max(ser_l),div_ser)
		for i in xrange(5,len(internal)):
			if type(internal[i][0].value)!=float:
				break
			else:
				if type(internal[i][sub].value)==float:
					val1 = internal[i][sub].value
				else:
					val1 = string.replace(internal[i][sub].value,u'\xa0',u'')
					val1 = float(val1) if val1!=u'' else 0.0
				
				if type(external[i][sub].value)==float:
					val2 = external[i][sub].value
				else:
					val2 = string.replace(external[i][sub].value,u'\xa0',u'')
					val2 = float(val2) if val2!=u'' else 0.0

				if type(series1[i][sub].value)==float:
					val3 = series1[i][sub].value
				else:
					val3 = string.replace(series1[i][sub].value,u'\xa0',u'')
					val3 = float(val3) if val3!=u'' else 0.0
				
				if type(series2[i][sub].value)==float:
					val4 = series2[i][sub].value
				else:
					val4 = string.replace(series2[i][sub].value,u'\xa0',u'')
					val4 = float(val4) if val4!=u'' else 0.0
				try:
					attendance = (val1-10-(val4*div_ser[1]/div_ser[0])-(val3*(30-div_ser[1])/50.0))/10
					data[string.replace(string.replace(internal[i][1].value,u'\xa0',u''),u' ',u'')+'_'+final[:4]+'_'+head[sub].value]=[subj[head[sub].value][0],subj[head[sub].value][1],float(val1)/50.0,val3/50.0,val4/div_ser[0],attendance]
					target[string.replace(string.replace(internal[i][1].value,u'\xa0',u''),u' ',u'')+'_'+final[:4]+'_'+head[sub].value]=float(val2)/100.0
				except KeyError as e:
					print string.replace(string.replace(internal[i][1].value,u'\xa0',u''),u' ',u'')+'_'+final[:4]+'_'+head[sub].value
					with open('missing.csv', 'a+') as csv_file:
						writer = csv.writer(csv_file, delimiter='\n')
						writer.writerow([string.replace(string.replace(internal[i][1].value,u'\xa0',u''),u' ',u'')+'_'+final[:4]+'_'+head[sub].value])

		for sub in xrange(no_subs[sem][0]+2,no_subs[sem][0]+2+no_subs[sem][1]):
			for i in xrange(5,len(internal)):
				if type(internal[i][0].value)!=float:
					break
				else:
					if type(internal[i][sub].value)==float:
						val1 = internal[i][sub].value
					else:
						val1 = string.replace(internal[i][sub].value,u'\xa0',u'')
						val1 = float(val1) if val1!=u'' else 0.0
					
					if type(external[i][sub].value)==float:
						val2 = external[i][sub].value
					else:
						val2 = string.replace(external[i][sub].value,u'\xa0',u'')
						val2 = float(val2) if val2!=u'' else 0.0

					if type(series1[i][sub].value)==float:
						val3 = series1[i][sub].value
					else:
						val3 = string.replace(series1[i][sub].value,u'\xa0',u'')
						val3 = float(val3) if val3!=u'' else 0.0
					
					if type(series2[i][sub].value)==float:
						val4 = series2[i][sub].value
					else:
						val4 = string.replace(series2[i][sub].value,u'\xa0',u'')
						val4 = float(val4) if val4!=u'' else 0.0
					try:
						if val3==0.0 and val4 ==0.0:
							print "val1 and val2 is 0 at : "+string.replace(string.replace(internal[i][1].value,u'\xa0',u''),u' ',u'')+'_'+final[:4]+'_'+head[sub].value
						else:
							val3/=25
							if val4 == 0.0:
								val4 = val3

						attendance = (val1-(val4*20)-(val3*20))/10
						data[string.replace(string.replace(internal[i][1].value,u'\xa0',u''),u' ',u'')+'_'+final[:4]+'_'+head[sub].value] = [subj[head[sub].value][0],subj[head[sub].value][1],float(val1)/50.0,val3,val4,attendance]
						target[string.replace(string.replace(internal[i][1].value,u'\xa0',u''),u' ',u'')+'_'+final[:4]+'_'+head[sub].value]=float(val2)/100.0

					except KeyError as e:
						print string.replace(string.replace(internal[i][1].value,u'\xa0',u''),u' ',u'')+'_'+final[:4]+'_'+head[sub].value
						with open('missing.csv', 'a+') as csv_file:
							writer = csv.writer(csv_file, delimiter='\n')
							writer.writerow([string.replace(string.replace(internal[i][1].value,u'\xa0',u''),u' ',u'')+'_'+final[:4]+'_'+head[sub].value])

pickle.dump( data, open( "data.p", "wb" ))
pickle.dump( target, open( "target.p", "wb" ))